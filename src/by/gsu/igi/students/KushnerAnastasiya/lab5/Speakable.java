package by.gsu.igi.students.KushnerAnastasiya.lab5;

/**
 * Created by KushnerAnastasiya.
 */
public interface Speakable {
    void speak();
}
