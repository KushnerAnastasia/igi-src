package by.gsu.igi.students.KushnerAnastasiya.lab5;

/**
 * Created by KushnerAnastasiya.
 */
public class Dog implements Speakable {
    @Override
    public void speak() {
        System.out.println("Vuf-vuf");
    }
}
