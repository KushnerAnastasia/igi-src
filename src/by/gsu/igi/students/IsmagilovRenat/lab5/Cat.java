package by.gsu.igi.students.IsmagilovRenat.lab5;

/**
 * Created by IsmagilovRenat.
 */
public class Cat implements Speakable {
    @Override
    public void speak() {
        System.out.println("meow-meow-meow");
    }
}