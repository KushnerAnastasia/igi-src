package by.gsu.igi.students.IsmagilovRenat.lab5;

/**
 * Created by IsmagilovRenat.
 */
public interface Speakable {
    void speak();
}