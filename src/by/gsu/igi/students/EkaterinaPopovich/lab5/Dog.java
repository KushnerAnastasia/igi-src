package by.gsu.igi.students.EkaterinaPopovich.lab5;

/**
 * Created by epopovich on 15.12.16.
 */
public class Dog implements Speakable {
    @Override
    public void speak() {
        System.out.println("Woof-woof");
    }
}
