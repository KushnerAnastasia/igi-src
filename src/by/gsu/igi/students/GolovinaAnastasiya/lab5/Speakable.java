package by.gsu.igi.students.GolovinaAnastasiya.lab5;

/**
 * Created by agolovina on 22.12.16.
 */
public class Speakable {
    public interface Speakable {
        void speak();
    }
}