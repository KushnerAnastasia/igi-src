package by.gsu.igi.students.DruzenkoMax.lab5;

/**
 * Created by Druzenko Max.
 */
public class Dog implements Speakable {
    @Override
    public void speak() {
        System.out.println("Vuf-vuf");
    }
}
