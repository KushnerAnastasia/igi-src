package by.gsu.igi.lectures.lecture02;

public class StaticDemo {
    public static Dog sharedDog;
    private Dog personalDog;

    public static void main(String[] args) {
        StaticDemo.sharedDog = new Dog();
        StaticDemo.sharedDog.name = "Homeless";

        StaticDemo owner1 = new StaticDemo();
        owner1.personalDog = new Dog();
        owner1.personalDog.name = "Snoopy";

        StaticDemo owner2 = new StaticDemo();
        owner2.personalDog = new Dog();
        owner2.personalDog.name = "Spoofy";

        System.out.println(owner2.sharedDog.name);
        System.out.println(owner1.sharedDog == owner2.sharedDog);

        System.out.println("owner1 owns " + owner1.personalDog.name);
        System.out.println("owner2 owns " + owner2.personalDog.name);
    }
}
