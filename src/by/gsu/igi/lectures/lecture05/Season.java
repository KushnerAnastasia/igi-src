package by.gsu.igi.lectures.lecture05;

import by.gsu.igi.lectures.lecture04.Todo;

/**
 * @author Evgeniy Myslovets
 */
public enum Season {
    WINTER,
    SPRING,
    SUMMER,
    AUTUMN;

    @Todo("not implemented")
    public Season next() {
        return values()[(ordinal() + 1) % values().length];
    }
}