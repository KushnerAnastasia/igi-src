package by.gsu.igi.lectures.lecture05;

public abstract class Animal implements Cloneable {
    private int age;

    public int getAge() {
        return age;
    }

    public void becomeOlder() {
        age++;
    }

    @Override
    protected Animal clone() {
        try {
            return (Animal) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
